# Chapter1-1.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

import tkinter as tk

root = tk.Tk()

label = tk.Label(root, text="Hello World", padx=10, pady=10)
label.pack()

root.mainloop()
