# win_prog.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import messagebox


class Win_Program:
    def __init__(self):
        self.master = Tk()
        self.master.state("withdrawn")  # 窗口最大化
        self.master.title("demo")
        self.master.grid()
        self.databases = list()
        self.var = StringVar()
        for i in range(40):
            self.databases.append("database"+str(i))

    def get_db_configure(self):
        top = Toplevel(self.master)
        top.title("数据库配置")
        top.resizable(0, 0)  # 大小不可变
        # 创建的Toplevel对象 在最上层
        top.attributes("-topmost", 1)
        top.wm_attributes("-topmost", 1)
        top.grid()
        sb = Scrollbar(top)
        sb.grid(row=0, rowspan=20, sticky=E+NS, padx=10, pady=5, column=1)
        lb = Listbox(top, listvariable=self.var, width=65,
                     yscrollcommand=sb.set, selectmode=SINGLE, height=20)
        lb.bind(sequence='<Double-Button-1>',
                func=self.handler_adaptor(self.handler, lb=lb, top=top))
        for i in range(len(self.databases)):
            lb.insert(0, self.databases[i])
        lb.grid(row=0, rowspan=20, column=0, padx=5, pady=5)
        # Listbox 滚动时，通过lb.yview方法 通知到 Scrollbar 组件
        sb.config(command=lb.yview)

        return top

    def widget_to_center(self, master, width, height):
        # 获取屏幕长/宽
        self.width = self.master.winfo_screenwidth()
        self.height = self.master.winfo_screenheight()
        x = self.width / 2 - width / 2
        y = self.height / 2 - height / 2
        master.geometry('%dx%d+%d+%d' % (width, height, x, y))
        master.grid()
        print(self.width, self.height, x, y)

    def handler(self, event, top, lb):
        """事件处理函数"""
        content = lb.get(lb.curselection())
        return messagebox.showinfo(title="Hey, you got me!", message="I am {0}".format(content), parent=top)

    def handler_adaptor(self, fun,  **kwds):
        """事件处理函数的适配器，相当于中介，那个event是从那里来的呢，我也纳闷，这也许就是python的伟大之处吧"""
        return lambda event, fun=fun, kwds=kwds: fun(event, **kwds)


if __name__ == "__main__":
    win_program = Win_Program()
    win_program.widget_to_center(win_program.get_db_configure(), 500, 400)
    mainloop()
